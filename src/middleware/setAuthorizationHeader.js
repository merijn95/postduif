import axios from '../axios/'

/**
 * Set the 'Authorization' HTTP header.
 * @param {*} param0 
 */
export default function({ store, next }) {
    axios.interceptors.request.use(function (request) {
        const token = store.getters['account/sessionToken']
        request.headers['Authorization'] = 'Bearer ' + token

        return request
    }, (error) => Promise.reject(error))

    return next()
}