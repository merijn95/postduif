import axios from '../axios'

/**
 * Enable the auth interceptor.
 *
 * We use this middleware to check if the API still allows the authorization token.
 * @param {*} param0 
 */
export default function({ next, store }) {
    axios.interceptors.response.use(function (response) {
        if (response.status === 401)  {
            // Current token in the store is not useful anymore. Log the user out. 
            store.dispatch('account/logout')
        }       

        return response
    }, (error) => Promise.reject(error))
    
    return next()
}