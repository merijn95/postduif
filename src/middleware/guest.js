export default function({ next, store }) {
    if (store.getters['account/isLoggedIn']) {
        return next({
            name: 'Dashboard'
        })
    }

    return next()
}