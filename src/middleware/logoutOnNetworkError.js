import axios from "../axios"

export default function({ next, store}) {
    axios.interceptors.response.use(
        response => response,
        error => {
            store.dispatch('account/logout')
            Promise.reject(error)
        }
    )

    return next()
}