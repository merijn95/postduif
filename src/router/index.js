import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import guest from '../middleware/guest'

import middlewarePipeline from './middlewarePipeline'
import { authMiddlewareGroup } from './middlewareGroup'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      middleware: [
       guest
      ]
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue'),
    meta: {
      middleware: [
        guest
      ]
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue'),
    meta: {
      middleware: authMiddlewareGroup
    }
  },
  {
    path: '/inbox/create',
    name: 'createInbox',
    component: () => import('../views/Inbox/Create.vue'),
    meta: {
      middleware: authMiddlewareGroup
    }
  },
  {
    path: '/inbox/:id',
    name: 'Inbox',
    component: () => import('../views/Inbox.vue'),
    meta: {
      middleware: authMiddlewareGroup
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }

  const currentMiddleware = to.meta.middleware[0]
  const context = { to, from, next, store, router}

  return currentMiddleware({...context, next: middlewarePipeline(context, to.meta.middleware, 1) })
})

export default router
