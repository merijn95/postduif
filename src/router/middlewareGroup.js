import setAuthorizationHeader from "../middleware/setAuthorizationHeader";
import enableAuthInterceptor from "../middleware/enableAuthInterceptor";
import authenticated from "../middleware/authenticated";
import logoutOnNetworkError from "../middleware/logoutOnNetworkError";

export const authMiddlewareGroup = [ setAuthorizationHeader,  enableAuthInterceptor, logoutOnNetworkError, authenticated ]