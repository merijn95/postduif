export class ValidationError extends Error {
    constructor(errors) {
        let message = '' 
        for (const property in errors) {
            if (message !== '') {
                message += '<br />'
            }

            const value = errors[property]
            message += value
        }
        
        super(message)
        this.name = this.constructor.name
    }
}