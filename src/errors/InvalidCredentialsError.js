export class InvalidCredentialsError extends Error {
    constructor(message) {
        super(message)
    }
}