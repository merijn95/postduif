export var errorMixin = {
    data() {
        return {
            error: ''
        }
    },

    onError(err) {
        this.error = err
    }
}
