export const LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL'
export const SET_TOKEN = 'SET_TOKEN'
export const RESET_AUTH = 'RESET_AUTH'