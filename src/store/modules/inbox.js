import axios from "../../axios"
import { INBOX_CREATED } from "../../constants/inbox"
import { ValidationError } from "../../errors/ValidationError"

export default {
    namespaced: true,
    actions: {
        async createAsync(_, data) {
            const response = await axios({
                method: 'post',
                url: '/inbox',
                data: data
            })

            if (response.status !== INBOX_CREATED) {
                throw new ValidationError(response.data.errors)
            }
        },

        async getInboxesAsync() {
            const response = await axios({
                method: 'get',
                url: '/inbox'
            })

            return response.data.inboxes
        },
    }
}