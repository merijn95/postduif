import { LOGIN_SUCCESSFUL, SET_TOKEN, RESET_AUTH } from './types'
import { InvalidCredentialsError } from '../../errors/InvalidCredentialsError'
import axios from '../../axios'
import { TokenNotFoundError } from '../../errors/TokenNotFoundError'
import { HTTP_CONFLICT, ACCOUNT_CREATED, INVALID_CREDENTIALS_STATUS } from '../../constants/account'
import router from '../../router'

export default {
    namespaced: true,

    state: {
        isLoggedIn: false,
        sessionToken: ''
    },

    getters: {
        isLoggedIn: state => state.isLoggedIn,
        sessionToken: state => state.sessionToken
    },

    mutations: {
        [LOGIN_SUCCESSFUL] (state) {
            state.isLoggedIn = true
        },

        [SET_TOKEN] (state, token) {
            state.sessionToken = token
        },

        [RESET_AUTH] (state) {
            state.isLoggedIn = false
            state.sessionToken = ''
        }
    },

    actions: {
        async loginAsync({ commit }, data) {
            const response = await axios({
                method: 'post', 
                url: '/auth',
                data: data 
            })

            if (response.status === INVALID_CREDENTIALS_STATUS) {
                throw new InvalidCredentialsError('Username and password combination not found')
            }

            const token = response.data.token
            if (token === undefined) {
                throw new TokenNotFoundError('Token not found in API response')
            }

            commit(SET_TOKEN, token)
            commit(LOGIN_SUCCESSFUL)

            router.push({ name: 'Dashboard' })
        },

        async registerAndLoginAsync({ dispatch }, data) {
            const response = await axios({
                method: 'post', 
                url: '/register',
                data: data 
            })
            
            if (response.status === HTTP_CONFLICT) {
                // TODO: Assume that the response, in this case, contains an error message.
                //
                // It's okay to assume - for now - that HTTP_CONFLICT means that the email address is already
                // in use; the registration process is not complicated enough to complain about the other input fields.
                throw new Error('Email address is already in use')
            }

            if (response.status !== ACCOUNT_CREATED) {
                throw new Error(response.data.message)
            }

            // Map data to a 'model' that login understands.
            await dispatch('loginAsync', { email: data.email, password: data.password })
        },

        logout({ commit }) {
            commit(RESET_AUTH)

            router.push({ name: 'Login' })
        }
    }
}