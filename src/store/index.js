import Vue from 'vue'
import Vuex from 'vuex'
import accountModule from './modules/account'
import inboxModule from './modules/inbox'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [createPersistedState()],
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    account: accountModule,
    inbox: inboxModule
  }
})
