import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import Vuelidate from 'vuelidate'
import store from './store'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

Vue.config.productionTip = false

Vue.config.errorHandler = function (err, vm, info) {
  if (vm.$options.onError) {
    const message = err.message
    vm.$options.onError.call(vm, message, err, vm, info)
    return
  } 

  console.error(err)
}

Vue.use(Vuelidate)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
